package kz.mn.awesomeproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AwesomeprojectApplication {

	public static void main(String[] args) {
		SpringApplication.run(AwesomeprojectApplication.class, args);
	}

}
