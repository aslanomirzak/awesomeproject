package kz.mn.awesomeproject.web.api;

import jakarta.validation.Valid;
import kz.mn.awesomeproject.business.dto.answerDto.AnswerRequestDto;
import kz.mn.awesomeproject.business.dto.answerDto.AnswerResponseDto;
import kz.mn.awesomeproject.business.dto.courseDto.CourseRequestDto;
import kz.mn.awesomeproject.business.dto.courseDto.CourseResponseDto;
import kz.mn.awesomeproject.business.dto.curatorDto.CuratorRequestDto;
import kz.mn.awesomeproject.business.dto.curatorDto.CuratorResponseDto;
import kz.mn.awesomeproject.business.dto.studentDto.StudentRequestDto;
import kz.mn.awesomeproject.business.dto.studentDto.StudentResponseDto;
import kz.mn.awesomeproject.business.dto.taskDto.TaskRequestDto;
import kz.mn.awesomeproject.business.dto.taskDto.TaskResponseDto;
import kz.mn.awesomeproject.business.dto.teacherDto.TeacherRequestDto;
import kz.mn.awesomeproject.business.dto.teacherDto.TeacherResponseDto;
import kz.mn.awesomeproject.business.facade.BusinessObjectFacade;
import kz.mn.awesomeproject.business.facade.ReadOnlyFacade;
import kz.mn.awesomeproject.business.facade.impl.ReadOnlyFacadeImpl;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
public class MyController {
    private final BusinessObjectFacade businessObjectFacade;
    private final ReadOnlyFacade readOnlyFacade;

    @PostMapping("/student")
    public StudentResponseDto createStudent(@RequestBody @Valid StudentRequestDto studentRequestDto){
        return businessObjectFacade.createStudent(studentRequestDto);
    }

    @GetMapping("/student/{id}")
    public StudentResponseDto getStudent (@PathVariable Integer id){
        return readOnlyFacade.getStudent(id);
    }

    @PutMapping("/student")
    public StudentResponseDto updateStudent(@RequestBody @Valid StudentRequestDto studentRequestDto){
        return businessObjectFacade.updateStudent(studentRequestDto);
    }

    @DeleteMapping("/student/{id}")
    public void deleteStudent (@PathVariable Integer id) {
        businessObjectFacade.deleteStudent(id);
    }

    @PostMapping("/curator")
    public CuratorResponseDto createCurator(@RequestBody @Valid CuratorRequestDto curatorRequestDto) {
        return businessObjectFacade.createCurator(curatorRequestDto);
    }

    @GetMapping("/curator/{id}")
    public CuratorResponseDto getCurator (@PathVariable Integer id){
        return readOnlyFacade.getCurator(id);
    }

    @PutMapping("/curator")
    public CuratorResponseDto updateCurator(@RequestBody @Valid CuratorRequestDto curatorRequestDto){
        return businessObjectFacade.updateCurator(curatorRequestDto);
    }

    @DeleteMapping("/curator/{id}")
    public void deleteCurator (@PathVariable Integer id) {
        businessObjectFacade.deleteCurator(id);
    }

    @PostMapping("/course")
    public CourseResponseDto createCourse(@RequestBody @Valid CourseRequestDto courseRequestDto) {
        return businessObjectFacade.createCourse(courseRequestDto);
    }

    @GetMapping("/course/{id}")
    public CourseResponseDto getCourse(@PathVariable Integer id){
        return readOnlyFacade.getCourse(id);
    }

    @PutMapping("/course")
    public CourseResponseDto updateCourse(@RequestBody @Valid CourseRequestDto courseRequestDto){
        return businessObjectFacade.updateCourse(courseRequestDto);
    }

    @DeleteMapping("/course/{id}")
    public void deleteCourse (@PathVariable Integer id) {
        businessObjectFacade.deleteCourse(id);
    }

    @PostMapping("/teacher")
    public TeacherResponseDto createTeacher(@RequestBody @Valid TeacherRequestDto teacherRequestDto) {
        return businessObjectFacade.createTeacher(teacherRequestDto);
    }
    @GetMapping("/teacher/{id}")
    public TeacherResponseDto getTeacher (@PathVariable Integer id){
        return readOnlyFacade.getTeacher(id);
    }

    @PutMapping("/teacher")
    public TeacherResponseDto updateTeacher(@RequestBody @Valid TeacherRequestDto teacherRequestDto){
        return businessObjectFacade.updateTeacher(teacherRequestDto);
    }

    @DeleteMapping("/teacher/{id}")
    public void deleteTeacher (@PathVariable Integer id){
        businessObjectFacade.deleteTeacher(id);
    }
//    @PostMapping ("/task")
//    public TaskResponseDto createTask(@RequestBody @Valid TaskRequestDto  taskRequestDto){
//        return businessObjectFacade.createTask(taskRequestDto);
//    }
//    @GetMapping("/task/{id}")
//    public TaskResponseDto getTask (@PathVariable  Integer id){
//        return readOnlyFacade.getTask(id);
//    }
//    @PutMapping ("/task")
//    public TaskResponseDto updateTask (@RequestBody @Valid TaskRequestDto taskRequestDto){
//        return businessObjectFacade.updateTask(taskRequestDto);
//    }
//
//    @DeleteMapping("/task/{id}")
//        public void deleteTask(@PathVariable Integer id){
//        businessObjectFacade.deleteTask(id);
//    }
//    @PostMapping("/answer")
//    public AnswerResponseDto createAnswer (@RequestBody @Valid AnswerRequestDto answerRequestDto){
//        return businessObjectFacade.createAnswer(answerRequestDto);
//    }
//
//    @GetMapping("/answer/{id}")
//    public AnswerResponseDto getAnswer(@PathVariable Integer id){
//        return readOnlyFacade.getAnswer(id);
//    }
//    @PutMapping("/answer")
//    public AnswerResponseDto updateAnswer (@RequestBody @Valid AnswerRequestDto answerRequestDto){
//        return businessObjectFacade.updateAnswer(answerRequestDto);
//    }
//
//    @DeleteMapping("/answer/{id}")
//    public void deleteAnswer (@PathVariable Integer id){
//        businessObjectFacade.deleteAnswer(id);
//    }


}




