package kz.mn.awesomeproject.web.api;

import jakarta.validation.Valid;
import kz.mn.awesomeproject.business.dto.taskDto.TaskRequestDto;
import kz.mn.awesomeproject.business.dto.taskDto.TaskResponseDto;
import kz.mn.awesomeproject.business.dto.teacherDto.TeacherRequestDto;
import kz.mn.awesomeproject.business.facade.BusinessObjectFacade;
import kz.mn.awesomeproject.business.facade.ReadOnlyFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class BusinessLogicController {

    private final BusinessObjectFacade businessObjectFacade;
    private final ReadOnlyFacade readOnlyFacade;

    @PostMapping("/task")
    public TaskResponseDto createTask (@RequestBody @Valid TaskRequestDto taskRequestDto,
                                       @RequestParam(name="teacherId") Integer teacherId,
                                       @RequestParam(name="courseId") Integer courseId) {
        return businessObjectFacade.createTask(taskRequestDto, teacherId, courseId);
    }
}
