package kz.mn.awesomeproject.dal.entity.student;

import jakarta.persistence.*;
import kz.mn.awesomeproject.dal.entity.course.CourseEntity;
import kz.mn.awesomeproject.dal.entity.curator.CuratorEntity;
import kz.mn.awesomeproject.dal.model.StudentJsonData;
import lombok.*;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.util.Set;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "student")
public class StudentEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id;

    @Builder.Default
    @JdbcTypeCode(SqlTypes.JSON)
    @Column(name = "data")
    private StudentJsonData data =  new StudentJsonData();

    @ManyToOne
    private CuratorEntity curator;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "student_courses",
            joinColumns = @JoinColumn(name = "student_entity_id"),
            inverseJoinColumns = @JoinColumn(name = "courses_id")
    )
    private Set<CourseEntity> courses;
}
