package kz.mn.awesomeproject.dal.entity.teacher;

import jakarta.persistence.*;
import kz.mn.awesomeproject.dal.entity.course.CourseEntity;
import kz.mn.awesomeproject.dal.model.TeacherJsonData;
import lombok.*;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.util.Set;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "teacher")
public class TeacherEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id;

    @Builder.Default
    @JdbcTypeCode(SqlTypes.JSON)
    @Column(name = "data")
    private TeacherJsonData data = new TeacherJsonData();

    @OneToMany(mappedBy = "teacher")
    private Set<CourseEntity> courses;
}
