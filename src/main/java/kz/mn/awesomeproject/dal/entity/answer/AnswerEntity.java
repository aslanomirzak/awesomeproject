package kz.mn.awesomeproject.dal.entity.answer;

import jakarta.persistence.*;
import kz.mn.awesomeproject.dal.entity.task.TaskEntity;
import kz.mn.awesomeproject.dal.model.AnswerJsonData;
import kz.mn.awesomeproject.dal.model.CourseJsonData;
import lombok.*;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name ="answer")
public class AnswerEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id;

    @Builder.Default
    @JdbcTypeCode(SqlTypes.JSON)
    @Column(name = "data")
    private AnswerJsonData data = new AnswerJsonData();

    @ManyToOne
    private TaskEntity task;
}
