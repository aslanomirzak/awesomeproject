package kz.mn.awesomeproject.dal.entity.curator;

import jakarta.persistence.*;
import kz.mn.awesomeproject.dal.model.CuratorJsonData;
import kz.mn.awesomeproject.dal.model.StudentJsonData;
import lombok.*;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "curator")
public class CuratorEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id;

    @Builder.Default
    @JdbcTypeCode(SqlTypes.JSON)
    @Column(name = "data")
    private CuratorJsonData data =  new CuratorJsonData();
}
