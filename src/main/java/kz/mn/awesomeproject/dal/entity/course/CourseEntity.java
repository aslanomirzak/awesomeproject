package kz.mn.awesomeproject.dal.entity.course;

import jakarta.persistence.*;
import kz.mn.awesomeproject.dal.entity.student.StudentEntity;
import kz.mn.awesomeproject.dal.entity.teacher.TeacherEntity;
import kz.mn.awesomeproject.dal.model.CourseJsonData;
import lombok.*;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.util.Set;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name ="course")
public class CourseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id;

    @Builder.Default
    @JdbcTypeCode(SqlTypes.JSON)
    @Column(name = "data")
    private CourseJsonData data = new CourseJsonData();

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "student_courses",
            joinColumns = @JoinColumn(name = "courses_id"),
            inverseJoinColumns = @JoinColumn(name = "student_entity_id")
    )
    private Set<StudentEntity> students;

    @ManyToOne
    private TeacherEntity teacher;





}
