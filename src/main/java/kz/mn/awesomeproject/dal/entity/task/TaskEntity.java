package kz.mn.awesomeproject.dal.entity.task;

import jakarta.persistence.*;
import kz.mn.awesomeproject.dal.entity.course.CourseEntity;
import kz.mn.awesomeproject.dal.model.StudentJsonData;
import kz.mn.awesomeproject.dal.model.TaskJsonData;
import lombok.*;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "task")
public class TaskEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id;

    @Builder.Default
    @JdbcTypeCode(SqlTypes.JSON)
    @Column(name = "data")
    private TaskJsonData data =  new TaskJsonData();

    @ManyToOne
    private CourseEntity course;




}
