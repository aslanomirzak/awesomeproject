package kz.mn.awesomeproject.dal.repository;

import kz.mn.awesomeproject.dal.entity.curator.CuratorEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CuratorRepository extends CrudRepository<CuratorEntity,Integer> {
}
