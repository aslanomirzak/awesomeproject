package kz.mn.awesomeproject.dal.repository;
import kz.mn.awesomeproject.dal.entity.course.CourseEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepository extends CrudRepository<CourseEntity,Integer> {
}

