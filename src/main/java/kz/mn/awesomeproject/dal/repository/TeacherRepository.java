package kz.mn.awesomeproject.dal.repository;

import kz.mn.awesomeproject.dal.entity.teacher.TeacherEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeacherRepository extends CrudRepository <TeacherEntity, Integer> {

}
