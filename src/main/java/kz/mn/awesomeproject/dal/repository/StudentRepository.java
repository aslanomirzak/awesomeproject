package kz.mn.awesomeproject.dal.repository;

import kz.mn.awesomeproject.dal.entity.student.StudentEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends CrudRepository<StudentEntity,Integer> {

}
