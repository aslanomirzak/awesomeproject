package kz.mn.awesomeproject.dal.repository;

import kz.mn.awesomeproject.dal.entity.answer.AnswerEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnswerRepository extends CrudRepository <AnswerEntity,Integer> {
}
