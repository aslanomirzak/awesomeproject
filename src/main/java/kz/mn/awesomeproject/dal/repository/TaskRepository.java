package kz.mn.awesomeproject.dal.repository;

import kz.mn.awesomeproject.dal.entity.task.TaskEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TaskRepository  extends CrudRepository <TaskEntity,Integer> {
}
