package kz.mn.awesomeproject.business.mapper;

import kz.mn.awesomeproject.business.dto.answerDto.AnswerRequestDto;
import kz.mn.awesomeproject.business.dto.answerDto.AnswerResponseDto;
import kz.mn.awesomeproject.dal.entity.answer.AnswerEntity;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class AnswerMapper {
    public AnswerEntity getAnswerEntityFromRequestDto(AnswerRequestDto answerRequestDto) {
        if (answerRequestDto == null) {
            return null;
        }
        AnswerEntity answerEntity = AnswerEntity.builder()
                .id(answerRequestDto.getId())
                .data(answerRequestDto.getData())
                .build();
        return answerEntity;
    }

    public AnswerResponseDto getAnswerResponseDtoFromEntity(AnswerEntity answerEntity) {
        if (answerEntity == null) {
            return null;
        }
        AnswerResponseDto answerResponseDto = AnswerResponseDto.builder()
                .id(answerEntity.getId())
                .data(answerEntity.getData())
                .build();

        return answerResponseDto;

    }
}
