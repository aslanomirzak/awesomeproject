package kz.mn.awesomeproject.business.mapper;

import kz.mn.awesomeproject.business.dto.studentDto.StudentRequestDto;
import kz.mn.awesomeproject.business.dto.teacherDto.TeacherRequestDto;
import kz.mn.awesomeproject.business.dto.teacherDto.TeacherResponseDto;
import kz.mn.awesomeproject.dal.entity.teacher.TeacherEntity;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class TeacherMapper {
    public TeacherEntity getTeacherEntityFromRequestDto (TeacherRequestDto teacherRequestDto) {
        if (teacherRequestDto == null) {
            return null;
        }
        TeacherEntity teacherEntity = TeacherEntity.builder()
                .id(teacherRequestDto.getId())
                .data(teacherRequestDto.getData())
                .build();
        return teacherEntity;
    }

    public TeacherResponseDto getTeacherResponseDtoFromEntity (TeacherEntity teacherEntity){
        if (teacherEntity ==null){
            return null;
        }
        TeacherResponseDto teacherResponseDto = TeacherResponseDto.builder()
                .id(teacherEntity.getId())
                .data(teacherEntity.getData())
                .build();
        return teacherResponseDto;
    }
}
