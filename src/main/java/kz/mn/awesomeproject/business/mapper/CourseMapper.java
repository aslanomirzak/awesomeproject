package kz.mn.awesomeproject.business.mapper;

import kz.mn.awesomeproject.business.dto.courseDto.CourseRequestDto;

import kz.mn.awesomeproject.business.dto.courseDto.CourseResponseDto;

import kz.mn.awesomeproject.business.dto.studentDto.StudentRequestDto;
import kz.mn.awesomeproject.dal.entity.course.CourseEntity;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class CourseMapper {
    public CourseEntity getCourseEntityFromRequestDto (CourseRequestDto courseRequestDto) {
        if(courseRequestDto == null) {
            return null;
        }
        CourseEntity courseEntity = CourseEntity.builder()
                .id(courseRequestDto.getId())
                .data(courseRequestDto.getData())
                .build();
        return courseEntity;
    }


    public CourseResponseDto getCourseResponseDtoFromEntity (CourseEntity courseEntity) {
        if (courseEntity==null) {
            return null;
        }
        CourseResponseDto  courseResponseDto = CourseResponseDto.builder()
                .id(courseEntity.getId())
                .data(courseEntity.getData())
                .build();
        return courseResponseDto;
    }

    public Set<CourseResponseDto> getCourseResponsesDtoFromEntities (Set<CourseEntity> courseEntities) {
        return courseEntities.stream().map(
                courseEntity -> getCourseResponseDtoFromEntity(courseEntity)
        ).collect(Collectors.toSet());
    }



}
