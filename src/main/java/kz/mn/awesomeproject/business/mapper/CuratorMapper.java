package kz.mn.awesomeproject.business.mapper;

import kz.mn.awesomeproject.business.dto.curatorDto.CuratorRequestDto;
import kz.mn.awesomeproject.business.dto.curatorDto.CuratorResponseDto;
import kz.mn.awesomeproject.business.dto.studentDto.StudentResponseDto;
import kz.mn.awesomeproject.dal.entity.curator.CuratorEntity;
import kz.mn.awesomeproject.dal.entity.student.StudentEntity;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class CuratorMapper {
    public CuratorEntity getCuratorEntityFromRequestDto (CuratorRequestDto curatorRequestDto) {
        if (curatorRequestDto == null) {
            return null;
        }
        CuratorEntity curatorEntity = CuratorEntity.builder()
                .id(curatorRequestDto.getId())
                .data(curatorRequestDto.getData())
                .build();
        return curatorEntity;
    }
    public CuratorResponseDto getCuratorResponseDtoFromEntity (CuratorEntity curatorEntity) {
        if (curatorEntity ==null){
            return null;
        }
        CuratorResponseDto curatorResponseDto = CuratorResponseDto.builder()
                .id(curatorEntity.getId())
                .data(curatorEntity.getData())
                .build();
        return curatorResponseDto;
    }
}

