package kz.mn.awesomeproject.business.mapper;

import kz.mn.awesomeproject.business.dto.studentDto.StudentRequestDto;
import kz.mn.awesomeproject.business.dto.studentDto.StudentResponseDto;
import kz.mn.awesomeproject.dal.entity.student.StudentEntity;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class StudentMapper {
    public StudentEntity getStudentEntityFromRequestDto(StudentRequestDto studentRequestDto) {
        if (studentRequestDto == null) {
            return null;
        }
        StudentEntity studentEntity = StudentEntity.builder()
                .id(studentRequestDto.getId())
                .data(studentRequestDto.getData())
                .build();
        return studentEntity;
    }

    public StudentResponseDto getStudentResponseDtoFromEntity(StudentEntity studentEntity) {
        if (studentEntity == null) {
            return null;
        }
        StudentResponseDto studentResponseDto = StudentResponseDto.builder()
                .id(studentEntity.getId())
                .data(studentEntity.getData())
                .build();

        return studentResponseDto;

    }

    public Set<StudentResponseDto> getStudentResponseDtosFromEntities(Set<StudentEntity> entities) {
        return entities.stream().map(
                entity -> getStudentResponseDtoFromEntity(entity)
        ).collect(Collectors.toSet());
    }


}


