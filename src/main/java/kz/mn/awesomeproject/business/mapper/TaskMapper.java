package kz.mn.awesomeproject.business.mapper;

import kz.mn.awesomeproject.business.dto.taskDto.TaskRequestDto;
import kz.mn.awesomeproject.business.dto.taskDto.TaskResponseDto;
import kz.mn.awesomeproject.business.dto.teacherDto.TeacherRequestDto;
import kz.mn.awesomeproject.dal.entity.task.TaskEntity;
import kz.mn.awesomeproject.dal.entity.teacher.TeacherEntity;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class TaskMapper {
    public TaskEntity getTaskEntityFromTaskRequestDto (TaskRequestDto taskRequestDto){
        if(taskRequestDto == null){
            return null;
        }
        TaskEntity taskEntity = TaskEntity.builder()
                .id(taskRequestDto.getId())
                .data(taskRequestDto.getData())
                .build();
        return taskEntity;
    }

    public TaskResponseDto getTaskResponseDtoFromEntity (TaskEntity taskEntity){
        if(taskEntity == null){
            return null;
        }
        TaskResponseDto taskResponseDto = TaskResponseDto.builder()
                .id(taskEntity.getId())
                .data(taskEntity.getData())
                .build();
        return taskResponseDto;
    }
}
