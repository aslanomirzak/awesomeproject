package kz.mn.awesomeproject.business.dto.curatorDto;

import kz.mn.awesomeproject.dal.model.CuratorJsonData;
import lombok.Data;

@Data
public class CuratorRequestDto {
    private Integer id;
    private CuratorJsonData data;
}
