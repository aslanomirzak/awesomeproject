package kz.mn.awesomeproject.business.dto.teacherDto;

import kz.mn.awesomeproject.dal.model.TeacherJsonData;
import lombok.Data;

@Data
public class TeacherRequestDto {
    private Integer id;
    private TeacherJsonData data;

}
