package kz.mn.awesomeproject.business.dto.curatorDto;

import kz.mn.awesomeproject.dal.model.CuratorJsonData;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CuratorResponseDto {
    private Integer id;
    private CuratorJsonData data;
}
