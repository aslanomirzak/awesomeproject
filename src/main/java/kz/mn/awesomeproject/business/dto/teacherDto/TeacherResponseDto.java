package kz.mn.awesomeproject.business.dto.teacherDto;

import kz.mn.awesomeproject.dal.model.TeacherJsonData;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TeacherResponseDto {
    private Integer id;
    private TeacherJsonData data;
}
