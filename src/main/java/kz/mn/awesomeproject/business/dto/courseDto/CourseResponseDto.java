package kz.mn.awesomeproject.business.dto.courseDto;

import kz.mn.awesomeproject.business.dto.studentDto.StudentResponseDto;
import kz.mn.awesomeproject.dal.model.CourseJsonData;
import lombok.Builder;
import lombok.Data;

import java.util.Set;

@Data
@Builder
public class CourseResponseDto {
    private Integer id;
    private CourseJsonData data;
    private Set<StudentResponseDto> students;
}
