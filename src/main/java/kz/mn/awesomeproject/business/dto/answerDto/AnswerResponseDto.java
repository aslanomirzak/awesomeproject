package kz.mn.awesomeproject.business.dto.answerDto;

import com.fasterxml.jackson.annotation.JsonInclude;
import kz.mn.awesomeproject.dal.model.AnswerJsonData;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AnswerResponseDto {
    private Integer id;
    private AnswerJsonData data;
}
