package kz.mn.awesomeproject.business.dto.taskDto;

import kz.mn.awesomeproject.dal.model.AnswerJsonData;
import kz.mn.awesomeproject.dal.model.TaskJsonData;
import lombok.Data;

@Data
public class TaskRequestDto {
    private Integer id;
    private TaskJsonData data;
}
