package kz.mn.awesomeproject.business.dto.courseDto;

import kz.mn.awesomeproject.dal.model.CourseJsonData;
import lombok.Data;

@Data
public class CourseRequestDto {
    private Integer id;
    private CourseJsonData data;
}
