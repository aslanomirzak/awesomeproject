package kz.mn.awesomeproject.business.dto.answerDto;

import kz.mn.awesomeproject.dal.model.AnswerJsonData;
import kz.mn.awesomeproject.dal.model.StudentJsonData;
import lombok.Data;

@Data
public class AnswerRequestDto {
    private Integer id;
    private AnswerJsonData data;
}
