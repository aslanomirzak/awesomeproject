package kz.mn.awesomeproject.business.dto.studentDto;

import com.fasterxml.jackson.annotation.JsonInclude;
import kz.mn.awesomeproject.business.dto.courseDto.CourseResponseDto;
import kz.mn.awesomeproject.business.dto.curatorDto.CuratorResponseDto;
import kz.mn.awesomeproject.dal.model.StudentJsonData;
import lombok.Builder;
import lombok.Data;

import java.util.Set;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)

public class StudentResponseDto {
    private Integer id;
    private StudentJsonData data;
    private CuratorResponseDto curator;
    private Set<CourseResponseDto> courses;
}
