package kz.mn.awesomeproject.business.dto.studentDto;

import jakarta.validation.GroupSequence;
import kz.mn.awesomeproject.dal.model.StudentJsonData;
import lombok.Data;

import java.util.Set;

@Data

public class StudentRequestDto {
    private Integer id;
    private StudentJsonData data;
    private Integer curatorId;
    private Set<Integer> courseIds;

}
