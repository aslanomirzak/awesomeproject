package kz.mn.awesomeproject.business.facade;

import kz.mn.awesomeproject.business.dto.answerDto.AnswerRequestDto;
import kz.mn.awesomeproject.business.dto.answerDto.AnswerResponseDto;
import kz.mn.awesomeproject.business.dto.courseDto.CourseRequestDto;
import kz.mn.awesomeproject.business.dto.courseDto.CourseResponseDto;
import kz.mn.awesomeproject.business.dto.curatorDto.CuratorRequestDto;
import kz.mn.awesomeproject.business.dto.curatorDto.CuratorResponseDto;
import kz.mn.awesomeproject.business.dto.studentDto.StudentRequestDto;
import kz.mn.awesomeproject.business.dto.studentDto.StudentResponseDto;
import kz.mn.awesomeproject.business.dto.taskDto.TaskRequestDto;
import kz.mn.awesomeproject.business.dto.taskDto.TaskResponseDto;
import kz.mn.awesomeproject.business.dto.teacherDto.TeacherRequestDto;
import kz.mn.awesomeproject.business.dto.teacherDto.TeacherResponseDto;

public interface BusinessObjectFacade {
     StudentResponseDto createStudent (StudentRequestDto studentRequestDto);
     StudentResponseDto updateStudent (StudentRequestDto studentRequestDto);
     void deleteStudent (Integer id);

     CuratorResponseDto createCurator (CuratorRequestDto curatorRequestDto);

     CuratorResponseDto updateCurator (CuratorRequestDto curatorRequestDto);

     void deleteCurator(Integer id);

     CourseResponseDto createCourse (CourseRequestDto courseRequestDto );
     CourseResponseDto updateCourse (CourseRequestDto courseRequestDto);
     void deleteCourse (Integer id);

     TaskResponseDto createTask (TaskRequestDto taskRequestDto, Integer teacherId, Integer courseId);
     TaskResponseDto updateTask (TaskRequestDto taskRequestDto);
     void deleteTask (Integer id);


     AnswerResponseDto createAnswer(AnswerRequestDto answerRequestDto);

     AnswerResponseDto updateAnswer (AnswerRequestDto answerRequestDto);
     void deleteAnswer (Integer id);

     TeacherResponseDto createTeacher (TeacherRequestDto teacherRequestDto);
    TeacherResponseDto updateTeacher(TeacherRequestDto teacherRequestDto);
     void deleteTeacher (Integer id);





}
