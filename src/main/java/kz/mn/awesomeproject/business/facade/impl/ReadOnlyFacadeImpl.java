package kz.mn.awesomeproject.business.facade.impl;

import kz.mn.awesomeproject.business.dto.answerDto.AnswerResponseDto;
import kz.mn.awesomeproject.business.dto.courseDto.CourseResponseDto;
import kz.mn.awesomeproject.business.dto.curatorDto.CuratorResponseDto;
import kz.mn.awesomeproject.business.dto.studentDto.StudentRequestDto;
import kz.mn.awesomeproject.business.dto.studentDto.StudentResponseDto;
import kz.mn.awesomeproject.business.dto.taskDto.TaskResponseDto;
import kz.mn.awesomeproject.business.dto.teacherDto.TeacherResponseDto;
import kz.mn.awesomeproject.business.facade.ReadOnlyFacade;
import kz.mn.awesomeproject.business.mapper.*;
import kz.mn.awesomeproject.business.service.*;
import kz.mn.awesomeproject.dal.entity.answer.AnswerEntity;
import kz.mn.awesomeproject.dal.entity.course.CourseEntity;
import kz.mn.awesomeproject.dal.entity.curator.CuratorEntity;
import kz.mn.awesomeproject.dal.entity.student.StudentEntity;
import kz.mn.awesomeproject.dal.entity.task.TaskEntity;
import kz.mn.awesomeproject.dal.entity.teacher.TeacherEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
@RequiredArgsConstructor  //
public class ReadOnlyFacadeImpl implements ReadOnlyFacade {

    private final StudentService studentService;
    private final StudentMapper studentMapper;

    private final CuratorService curatorService;

    private final CuratorMapper curatorMapper;

    private final CourseService courseService;

    private final CourseMapper courseMapper;

    private  final TaskService taskService;
    private final TaskMapper taskMapper;

    private final AnswerService answerService;
    private final AnswerMapper answerMapper;

    private final TeacherService teacherService;

    private  final TeacherMapper teacherMapper;
    @Override
    public StudentResponseDto getStudent(Integer id) {
        StudentEntity studentEntity = studentService.getStudent(id);
        return studentMapper.getStudentResponseDtoFromEntity(studentEntity);
    }

    @Override
    public CuratorResponseDto getCurator(Integer id) {
        CuratorEntity curatorEntity = curatorService.getCurator(id);
        return curatorMapper.getCuratorResponseDtoFromEntity(curatorEntity);
    }

    @Override
    public CourseResponseDto getCourse(Integer id) {
        CourseEntity courseEntity = courseService.getCourse(id);
        CourseResponseDto response =  courseMapper.getCourseResponseDtoFromEntity(courseEntity);
        Set<StudentResponseDto> studentResponseDtoSet = studentMapper.getStudentResponseDtosFromEntities(courseEntity.getStudents());
        response.setStudents(studentResponseDtoSet);
        return response;
    }

    @Override
    public TaskResponseDto getTask(Integer id) {
        TaskEntity taskEntity = taskService.getTask(id);
        return taskMapper.getTaskResponseDtoFromEntity(taskEntity);
    }

    @Override
    public AnswerResponseDto getAnswer(Integer id) {
        AnswerEntity answerEntity = answerService.getAnswer(id);
        return answerMapper.getAnswerResponseDtoFromEntity(answerEntity);
    }

    @Override
    public TeacherResponseDto getTeacher(Integer id) {
        TeacherEntity teacherEntity  = teacherService.getTeacher(id);
        return teacherMapper.getTeacherResponseDtoFromEntity(teacherEntity);
    }




}
