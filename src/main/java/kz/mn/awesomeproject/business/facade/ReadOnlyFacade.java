package kz.mn.awesomeproject.business.facade;


import jakarta.persistence.criteria.CriteriaBuilder;
import kz.mn.awesomeproject.business.dto.answerDto.AnswerResponseDto;
import kz.mn.awesomeproject.business.dto.courseDto.CourseResponseDto;
import kz.mn.awesomeproject.business.dto.curatorDto.CuratorResponseDto;
import kz.mn.awesomeproject.business.dto.studentDto.StudentResponseDto;
import kz.mn.awesomeproject.business.dto.taskDto.TaskResponseDto;
import kz.mn.awesomeproject.business.dto.teacherDto.TeacherResponseDto;
import kz.mn.awesomeproject.dal.entity.curator.CuratorEntity;

public interface ReadOnlyFacade {
    StudentResponseDto getStudent (Integer id);
    CuratorResponseDto getCurator (Integer id);
    CourseResponseDto getCourse (Integer id);
    TaskResponseDto getTask (Integer id);
    AnswerResponseDto getAnswer(Integer id);

    TeacherResponseDto getTeacher (Integer id);





}
