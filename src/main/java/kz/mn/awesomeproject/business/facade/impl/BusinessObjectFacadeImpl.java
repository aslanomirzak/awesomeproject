package kz.mn.awesomeproject.business.facade.impl;

import kz.mn.awesomeproject.business.dto.answerDto.AnswerRequestDto;
import kz.mn.awesomeproject.business.dto.answerDto.AnswerResponseDto;
import kz.mn.awesomeproject.business.dto.courseDto.CourseRequestDto;
import kz.mn.awesomeproject.business.dto.courseDto.CourseResponseDto;
import kz.mn.awesomeproject.business.dto.curatorDto.CuratorRequestDto;
import kz.mn.awesomeproject.business.dto.curatorDto.CuratorResponseDto;
import kz.mn.awesomeproject.business.dto.studentDto.StudentRequestDto;
import kz.mn.awesomeproject.business.dto.studentDto.StudentResponseDto;
import kz.mn.awesomeproject.business.dto.taskDto.TaskRequestDto;
import kz.mn.awesomeproject.business.dto.taskDto.TaskResponseDto;
import kz.mn.awesomeproject.business.dto.teacherDto.TeacherRequestDto;
import kz.mn.awesomeproject.business.dto.teacherDto.TeacherResponseDto;
import kz.mn.awesomeproject.business.facade.BusinessObjectFacade;
import kz.mn.awesomeproject.business.mapper.*;
import kz.mn.awesomeproject.business.service.*;
import kz.mn.awesomeproject.common.exception.BusinessException;
import kz.mn.awesomeproject.dal.entity.answer.AnswerEntity;
import kz.mn.awesomeproject.dal.entity.course.CourseEntity;
import kz.mn.awesomeproject.dal.entity.curator.CuratorEntity;
import kz.mn.awesomeproject.dal.entity.student.StudentEntity;
import kz.mn.awesomeproject.dal.entity.task.TaskEntity;
import kz.mn.awesomeproject.dal.entity.teacher.TeacherEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Set;


@Service
@RequiredArgsConstructor
public class BusinessObjectFacadeImpl implements BusinessObjectFacade {
    private final StudentService studentService;
    private final StudentMapper studentMapper;

    private final CuratorService curatorService;
    private final CuratorMapper curatorMapper;

    private final CourseService courseService;

    private final CourseMapper courseMapper;

    private  final TaskService taskService;
    private final TaskMapper taskMapper;

    private final AnswerService answerService;
    private final AnswerMapper answerMapper;

    private final TeacherService teacherService;

    private  final TeacherMapper teacherMapper;



    @Override
    public StudentResponseDto createStudent(StudentRequestDto studentRequestDto) {
        if (studentRequestDto.getId()!=null){
            throw new BusinessException("Student cannot be created with id");
        }
        StudentEntity studentEntity = studentMapper.getStudentEntityFromRequestDto(studentRequestDto);

        CuratorEntity curatorEntity = curatorService.getCurator(studentRequestDto.getCuratorId());
        studentEntity.setCurator(curatorEntity);

        Set<CourseEntity> studentCourses = courseService.getCourses(studentRequestDto.getCourseIds());
        studentEntity.setCourses(studentCourses);

        StudentEntity returnStudent = studentService.createStudent(studentEntity);

        StudentResponseDto response = studentMapper.getStudentResponseDtoFromEntity(returnStudent);

        CuratorResponseDto curatorResponseDto = curatorMapper.getCuratorResponseDtoFromEntity(returnStudent.getCurator());
        Set<CourseResponseDto> courseResponseDtos = courseMapper.getCourseResponsesDtoFromEntities(returnStudent.getCourses());

        response.setCurator(curatorResponseDto);
        response.setCourses(courseResponseDtos);
        return response;

    }

    @Override
    public StudentResponseDto updateStudent(StudentRequestDto studentRequestDto) {
        if (studentRequestDto.getId()==null){
            throw new BusinessException("Student cannot be updated without id");
        }
        StudentEntity studentEntity = studentMapper.getStudentEntityFromRequestDto(studentRequestDto);
        CuratorEntity curatorEntity = curatorService.getCurator(studentRequestDto.getCuratorId());
        studentEntity.setCurator(curatorEntity);
        Set<CourseEntity> studentCourses = courseService.getCourses(studentRequestDto.getCourseIds());
        studentEntity.setCourses(studentCourses);
        StudentEntity returnStudent = studentService.updateStudent(studentEntity);
        StudentResponseDto response = studentMapper.getStudentResponseDtoFromEntity(returnStudent);
        CuratorResponseDto curatorResponseDto = curatorMapper.getCuratorResponseDtoFromEntity(returnStudent.getCurator());
        Set<CourseResponseDto> courseResponseDtos = courseMapper.getCourseResponsesDtoFromEntities(returnStudent.getCourses());
        response.setCurator(curatorResponseDto);
        response.setCourses(courseResponseDtos);
        return response;
    }

    @Override
    public void deleteStudent(Integer id) {
        studentService.deleteStudent(id);

    }
    @Override
    public CuratorResponseDto createCurator(CuratorRequestDto curatorRequestDto) {
        if(curatorRequestDto.getId()!=null){
            throw  new BusinessException("Curator cannot be created with id");
        }
        CuratorEntity curatorEntity = curatorMapper.getCuratorEntityFromRequestDto(curatorRequestDto);
        CuratorEntity returnCurator = curatorService.createCurator(curatorEntity);
         return curatorMapper.getCuratorResponseDtoFromEntity(returnCurator);
    }

    @Override
    public CuratorResponseDto updateCurator(CuratorRequestDto curatorRequestDto) {
        if (curatorRequestDto.getId()==null){
            throw new BusinessException("Curator cannot be updated without id");
        }
        CuratorEntity curatorEntity = curatorMapper.getCuratorEntityFromRequestDto(curatorRequestDto);
        CuratorEntity returnCurator = curatorService.createCurator(curatorEntity);
        return curatorMapper.getCuratorResponseDtoFromEntity(returnCurator);
    }
    @Override
    public void deleteCurator(Integer id) {
        curatorService.deleteCurator(id);
    }

    @Override
    public CourseResponseDto createCourse(CourseRequestDto courseRequestDto) {
        if(courseRequestDto.getId()!=null){
            throw  new BusinessException("Course cannot be created with id");
        }
        CourseEntity courseEntity =courseMapper.getCourseEntityFromRequestDto(courseRequestDto);
        CourseEntity  returnCourse = courseService.createCourse(courseEntity);
        return courseMapper.getCourseResponseDtoFromEntity(returnCourse);
    }

    @Override
    public CourseResponseDto updateCourse(CourseRequestDto courseRequestDto) {
        if (courseRequestDto.getId()==null){
            throw new BusinessException("Course cannot be updated without id");
        }
        CourseEntity courseEntity = courseMapper.getCourseEntityFromRequestDto(courseRequestDto);
        CourseEntity returnCourse = courseService.createCourse(courseEntity);
        return courseMapper.getCourseResponseDtoFromEntity(returnCourse);
    }

    @Override
    public void deleteCourse(Integer id) {
        courseService.deleteCourse(id);
    }

    @Override
    public TaskResponseDto createTask(TaskRequestDto taskRequestDto, Integer teacherId, Integer courseId) {
        if(taskRequestDto.getId()!=null){
            throw  new BusinessException("Task cannot be created with id");
        }
        CourseEntity course = courseService.getCourse(courseId);
        TeacherEntity teacher = teacherService.getTeacher(teacherId);
        if (!teacher.getCourses().contains(course)) {
            throw new BusinessException("This course does not exit in teacher's courses");
        }
        TaskEntity taskEntity =taskMapper.getTaskEntityFromTaskRequestDto(taskRequestDto);
        taskEntity.setCourse(course);
        TaskEntity  returnTask = taskService.createTask(taskEntity);
        return taskMapper.getTaskResponseDtoFromEntity(returnTask);
    }

    @Override
    public TaskResponseDto updateTask(TaskRequestDto taskRequestDto) {
        if(taskRequestDto.getId()==null){
            throw  new BusinessException("Task cannot be created with id");
        }


        TaskEntity taskEntity =taskMapper.getTaskEntityFromTaskRequestDto(taskRequestDto);
        TaskEntity  returnTask = taskService.createTask(taskEntity);
        return taskMapper.getTaskResponseDtoFromEntity(returnTask);
    }

    @Override
    public void deleteTask(Integer id) {
        taskService.deleteTask(id);
    }

    @Override
    public AnswerResponseDto createAnswer(AnswerRequestDto answerRequestDto) {
        if(answerRequestDto.getId()!=null){
            throw new BusinessException("Answer cannot be created with id");
        }
        AnswerEntity answerEntity = answerMapper.getAnswerEntityFromRequestDto(answerRequestDto);
        AnswerEntity returnAnswer = answerService.createAnswer(answerEntity);
        return answerMapper.getAnswerResponseDtoFromEntity(returnAnswer);
    }

    @Override
    public AnswerResponseDto updateAnswer(AnswerRequestDto answerRequestDto) {
        if(answerRequestDto.getId()==null){
            throw new BusinessException("Answer cannot be created with id");
        }
        AnswerEntity answerEntity = answerMapper.getAnswerEntityFromRequestDto(answerRequestDto);
        AnswerEntity returnAnswer = answerService.createAnswer(answerEntity);
        return answerMapper.getAnswerResponseDtoFromEntity(returnAnswer);
    }

    @Override
    public void deleteAnswer(Integer id) {
        answerService.deleteAnswer(id);
    }

    @Override
    public TeacherResponseDto createTeacher(TeacherRequestDto teacherRequestDto) {
        if(teacherRequestDto.getId()!=null){
            throw new BusinessException("Answer cannot be created with id");
        }
        TeacherEntity teacherEntity = teacherMapper.getTeacherEntityFromRequestDto(teacherRequestDto);
        TeacherEntity returnTeacher = teacherService.createTeacher(teacherEntity);
        return teacherMapper.getTeacherResponseDtoFromEntity(returnTeacher);
    }

    @Override
    public TeacherResponseDto updateTeacher(TeacherRequestDto teacherRequestDto) {
        if(teacherRequestDto.getId()==null){
            throw new BusinessException("Answer cannot be created with id");
        }
        TeacherEntity teacherEntity = teacherMapper.getTeacherEntityFromRequestDto(teacherRequestDto);
        TeacherEntity returnTeacher = teacherService.createTeacher(teacherEntity);
        return teacherMapper.getTeacherResponseDtoFromEntity(returnTeacher);
    }

    @Override
    public void deleteTeacher(Integer id) {
        teacherService.deleteTeacher(id);

    }
}
