package kz.mn.awesomeproject.business.service;

import kz.mn.awesomeproject.dal.entity.student.StudentEntity;
import kz.mn.awesomeproject.dal.entity.teacher.TeacherEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


public interface TeacherService{
    TeacherEntity createTeacher (TeacherEntity teacher);

    TeacherEntity getTeacher (Integer id);

    TeacherEntity updateTeacher (TeacherEntity teacher);

    void deleteTeacher(Integer id);
}
