package kz.mn.awesomeproject.business.service.impl;

import kz.mn.awesomeproject.business.service.StudentService;
import kz.mn.awesomeproject.common.exception.BusinessException;
import kz.mn.awesomeproject.dal.entity.student.StudentEntity;
import kz.mn.awesomeproject.dal.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {
    private final StudentRepository studentRepository;  //  StudentRepository injected

    @Override
    public StudentEntity createStudent(StudentEntity student) {
        return studentRepository.save(student);
    }

    @Override
    public StudentEntity getStudent(Integer id) {
        Optional<StudentEntity> optEntity = studentRepository.findById(id);
        return optEntity.orElseThrow(
                () -> new BusinessException("Entity with id "+ id + " does not exist")
        );
        // if entity not null  then return entity  -> else throw Ex
    }

    @Override
    public StudentEntity updateStudent(StudentEntity student) {
        return studentRepository.save(student);
    }

    @Override
    public void deleteStudent(Integer id) {
        if(!studentRepository.existsById(id)){
            throw new BusinessException("Student with " + id + "  not found");
        }
        studentRepository.deleteById(id);
    }
}
