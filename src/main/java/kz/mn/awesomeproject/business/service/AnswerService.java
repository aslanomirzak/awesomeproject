package kz.mn.awesomeproject.business.service;

import kz.mn.awesomeproject.dal.entity.answer.AnswerEntity;
import kz.mn.awesomeproject.dal.entity.student.StudentEntity;

public interface AnswerService {
    AnswerEntity createAnswer (AnswerEntity answer);
    AnswerEntity getAnswer (Integer id);
    AnswerEntity updateAnswer (AnswerEntity answer);
    void deleteAnswer (Integer id);
}
