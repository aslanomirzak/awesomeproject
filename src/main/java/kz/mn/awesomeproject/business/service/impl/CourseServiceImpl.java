package kz.mn.awesomeproject.business.service.impl;

import kz.mn.awesomeproject.business.service.CourseService;
import kz.mn.awesomeproject.common.exception.BusinessException;
import kz.mn.awesomeproject.dal.entity.course.CourseEntity;
import kz.mn.awesomeproject.dal.entity.curator.CuratorEntity;
import kz.mn.awesomeproject.dal.repository.CourseRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CourseServiceImpl implements CourseService {

    private final CourseRepository courseRepository;

    @Override
    public CourseEntity createCourse(CourseEntity course) {
        return courseRepository.save(course);
    }

    @Override
    public CourseEntity getCourse(Integer id) {
        Optional<CourseEntity> optionalCourseEntity = courseRepository.findById(id);
        return optionalCourseEntity.orElseThrow(
                () -> new BusinessException("Course with id "+ id + " does not exist")
        );
    }

    @Override
    public CourseEntity updateCourse(CourseEntity course) {
        return courseRepository.save(course);
    }

    @Override
    public void deleteCourse(Integer id) {
        if(!courseRepository.existsById(id)){
            throw new BusinessException("Course with " + id + "  not found");
        }
        courseRepository.deleteById(id);

    }

    @Override
    public Set<CourseEntity> getCourses(Set<Integer> ids) {
        return ids.stream().map(
                id -> getCourse(id)
        ).collect(Collectors.toSet());
    }
}
