package kz.mn.awesomeproject.business.service.impl;

import kz.mn.awesomeproject.business.service.TeacherService;
import kz.mn.awesomeproject.common.exception.BusinessException;
import kz.mn.awesomeproject.dal.entity.teacher.TeacherEntity;
import kz.mn.awesomeproject.dal.repository.TeacherRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TeacherServiceImpl implements TeacherService {

    private final TeacherRepository teacherRepository;

    @Override
    public TeacherEntity createTeacher(TeacherEntity teacher) {
        return teacherRepository.save(teacher);
    }

    @Override
    public TeacherEntity getTeacher(Integer id) {
        Optional<TeacherEntity> optionalTeacherEntity= teacherRepository.findById(id);
        return optionalTeacherEntity.orElseThrow(
                ()->new BusinessException("Teacher with id "+ id + " does not exist")
        );
    }

    @Override
    public TeacherEntity updateTeacher(TeacherEntity teacher) {
        return teacherRepository.save(teacher);
    }

    @Override
    public void deleteTeacher(Integer id) {
        if(!teacherRepository.existsById(id)){
            throw new BusinessException("Teacher with " + id + "  not found");
        }
        teacherRepository.deleteById(id);
    }
}
