package kz.mn.awesomeproject.business.service.impl;

import kz.mn.awesomeproject.business.service.TaskService;
import kz.mn.awesomeproject.common.exception.BusinessException;
import kz.mn.awesomeproject.dal.entity.task.TaskEntity;
import kz.mn.awesomeproject.dal.repository.TaskRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TaskServiceImpl  implements TaskService {

    private final TaskRepository taskRepository;

    @Override
    public TaskEntity createTask(TaskEntity task) {
        return taskRepository.save(task);
    }

    @Override
    public TaskEntity getTask(Integer id) {
        Optional<TaskEntity> optionalTaskEntity = taskRepository.findById(id);
        return optionalTaskEntity.orElseThrow(
                () -> new BusinessException("Task with id + " + id+ " does not exist" )
        );

    }

    @Override
    public TaskEntity updateTask(TaskEntity task) {
        return taskRepository.save(task);
    }

    @Override
    public void deleteTask(Integer id) {
        if(!taskRepository.existsById(id)) {
            throw new BusinessException("Task with " +id + " not found");
        }
        taskRepository.deleteById(id);
    }
}
