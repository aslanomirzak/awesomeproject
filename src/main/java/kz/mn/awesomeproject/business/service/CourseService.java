package kz.mn.awesomeproject.business.service;

import kz.mn.awesomeproject.dal.entity.course.CourseEntity;
import kz.mn.awesomeproject.dal.entity.curator.CuratorEntity;

import java.util.Set;

public interface CourseService {

    CourseEntity createCourse(CourseEntity course);
    CourseEntity getCourse (Integer id);
    CourseEntity updateCourse (CourseEntity course);
    void deleteCourse (Integer id);

    Set<CourseEntity> getCourses (Set<Integer> ids);


}
