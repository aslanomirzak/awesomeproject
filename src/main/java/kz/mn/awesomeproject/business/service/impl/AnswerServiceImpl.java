package kz.mn.awesomeproject.business.service.impl;

import kz.mn.awesomeproject.business.service.AnswerService;
import kz.mn.awesomeproject.common.exception.BusinessException;
import kz.mn.awesomeproject.dal.entity.answer.AnswerEntity;
import kz.mn.awesomeproject.dal.repository.AnswerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AnswerServiceImpl implements AnswerService {

    private final AnswerRepository answerRepository;
    @Override
    public AnswerEntity createAnswer(AnswerEntity answer) {
        return answerRepository.save(answer);
    }

    @Override
    public AnswerEntity getAnswer(Integer id) {
        Optional<AnswerEntity> optionalAnswerEntity  = answerRepository.findById(id);
        return optionalAnswerEntity.orElseThrow(
                ()-> new BusinessException("Answer with id " + id  + " does not exist")
        );
    }

    @Override
    public AnswerEntity updateAnswer(AnswerEntity answer) {
        return answerRepository.save(answer);
    }

    @Override
    public void deleteAnswer(Integer id) {
        if(!answerRepository.existsById(id)){
            throw new BusinessException("Answer with " + id + "not found");
        }
        answerRepository.deleteById(id);
    }
}
