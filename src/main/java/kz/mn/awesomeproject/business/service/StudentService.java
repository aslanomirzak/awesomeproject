package kz.mn.awesomeproject.business.service;


import kz.mn.awesomeproject.dal.entity.student.StudentEntity;

import java.util.Optional;

public interface StudentService {
    StudentEntity createStudent (StudentEntity student);

    StudentEntity getStudent (Integer id);

    StudentEntity updateStudent (StudentEntity student);

    void deleteStudent(Integer id);

}
