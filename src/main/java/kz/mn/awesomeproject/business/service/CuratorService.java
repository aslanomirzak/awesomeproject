package kz.mn.awesomeproject.business.service;

import jakarta.persistence.criteria.CriteriaBuilder;
import kz.mn.awesomeproject.dal.entity.curator.CuratorEntity;
import kz.mn.awesomeproject.dal.entity.student.StudentEntity;

public interface CuratorService {

    CuratorEntity createCurator (CuratorEntity curator);

    CuratorEntity getCurator (Integer id);

    CuratorEntity updateCurator (CuratorEntity curator);

    void deleteCurator(Integer id);
}

