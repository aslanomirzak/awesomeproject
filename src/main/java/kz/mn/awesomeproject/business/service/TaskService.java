package kz.mn.awesomeproject.business.service;

import kz.mn.awesomeproject.dal.entity.task.TaskEntity;

public interface TaskService {
    TaskEntity createTask  (TaskEntity task);
    TaskEntity getTask (Integer id);
    TaskEntity updateTask (TaskEntity task);
    void deleteTask(Integer id);

}
