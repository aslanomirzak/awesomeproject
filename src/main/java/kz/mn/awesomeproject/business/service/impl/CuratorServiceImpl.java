package kz.mn.awesomeproject.business.service.impl;

import kz.mn.awesomeproject.business.service.CuratorService;
import kz.mn.awesomeproject.business.service.StudentService;
import kz.mn.awesomeproject.common.exception.BusinessException;
import kz.mn.awesomeproject.dal.entity.curator.CuratorEntity;
import kz.mn.awesomeproject.dal.entity.student.StudentEntity;
import kz.mn.awesomeproject.dal.repository.CuratorRepository;
import kz.mn.awesomeproject.dal.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Service
@RequiredArgsConstructor
public class CuratorServiceImpl implements CuratorService {
    private final CuratorRepository curatorRepository;
    @Override
    public CuratorEntity createCurator(CuratorEntity curator) {
        return curatorRepository.save(curator);
    }

    @Override
    public CuratorEntity getCurator(Integer id) {
        Optional<CuratorEntity> optionalCuratorEntity = curatorRepository.findById(id);
        return optionalCuratorEntity.orElseThrow(
           () -> new BusinessException("Curator with id "+ id + " does not exist")
       );
    }

    @Override
    public CuratorEntity updateCurator(CuratorEntity curator) {
        return curatorRepository.save(curator);
    }

    @Override
    public void deleteCurator(Integer id) {
        if(!curatorRepository.existsById(id)){
            throw new BusinessException("Curator with " + id + "  not found");
        }
        curatorRepository.deleteById(id);
    }
}
